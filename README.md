# Welcome to my Data Science Repo

Anything that I work on that might be Data Science / Machine Learning related is going to be here.

# Structure
### deploy
Multipurpose Flask application hosting:
- 1) The Sock Market database where I can find socks using SQLAlchemy
- 2) Query the weather in the Grand Canyon using an API
- 3) Flask templates

### src
Lots of Data Science related modelling efforts in their own folders.
I'm trying things out to see what sparks my interest. 
Some highlights are:
- 1) gaussian - the start of making a plinko machine
- 2) linear_regression - where I started out modelling against data
- 3) sentiment analysis - an NLP analyzing amazon reviews
- 4) computer vision - starting out with the iris dataset



# Aspirations
- Spend some time with specific algorithms
    - Probably two: Linear regression, Random forest (via Decision Tree)
        - Output types
        - Input types
        - Data labelling
        - Use case
        -  How does it work, what's going on and how to train it
        -  Data preprocessing
        -  Specific use cases ie arguments
        -  Output visulisation
- Start getting data to train with
- Get more python unit testing into the repo
    - Comment code since this is visible
