
-- retreive list of employees  (first and last names)
-- include first/last of their manager

.tables

.schema employee

select *
-- select employeeId, firstName, lastName, managerId
from employee emp
where title = "Sales Person";

-- for each employeeId entry, join with the relevant managerId
-- both infos from the employee table
select emp.employeeId,
    emp.firstName, 
    emp.lastName, 
    emp.managerId,
    mng.firstName as managerFirstName, 
    mng.lastName as managerLastName
from employee emp inner join employee mng
    on emp.managerId = mng.employeeId
;


-- get a list of salespeople with 0 sales
-- use LEFT JOIN
.tables

select * 
from sales; 

select employeeId, firstName, lastName
from employee
where title = "Sales Person";

-- select s.salesId, 
--     s.employeeId, 
--     s.salesAmount,
--     emp.firstName, 
--     emp.lastName
-- from sales s inner join employee emp
--     on s.employeeId = emp.employeeId
-- ;

-- select *
select emp.employeeId, emp.firstName, emp.lastName, 
    s.salesAmount, s.salesId
from employee emp left join sales s
-- from sales s left join employee emp
    on emp.employeeId = s.employeeId
-- where emp.title = "Sales Person" 
where emp.title = "Sales Person" and s.salesAmount is NULL
;


-- get a list of all sales and all customers 
-- even if some data is missing
-- use a UNION
.tables

select * from customer;
select * from employee;
select * from inventory;
select * from model;
select * from sales;
select * from tbl1;

-- get info where there is customer data and sales data
-- inner join customer with sales
select cus.customerId, cus.email,
    s.salesId, s.employeeId, s.salesAmount
from customer cus inner join sales s
    on cus.customerId = s.customerId
union
-- get info from customer table which have no matching sales data
select cus.customerId, cus.email,
    s.salesId, s.employeeId, s.salesAmount
from customer cus left join sales s
    on cus.customerId = s.customerId
where s.salesId is null
union
-- get info from sales table that has no matching customer data
select cus.customerId, cus.email,
    s.salesId, s.employeeId, s.salesAmount
from sales s left join customer cus
    on cus.customerId = s.customerId
where cus.customerId is null
;
