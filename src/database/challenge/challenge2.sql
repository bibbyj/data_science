
-- report total number of cars sold by each employee

-- select emp.employeeId, emp.firstName, emp.lastName, sls.salesId, sls.salesAmount
-- from sales sls inner join employee emp
--     on sls.employeeId = emp.employeeId
-- order by emp.employeeId
-- ;

select * from sales;

select emp.employeeId, emp.firstName, emp.lastName, count(*) as CarsSold
from employee emp inner join sales sls
    on sls.employeeId = emp.employeeId
group by emp.employeeId
order by emp.employeeId
;

-- report least and most expensive cars sold by each employee
select emp.employeeId, emp.firstName, emp.lastName, 
    MIN(sls.salesAmount), MAX(sls.salesAmount)
from sales sls inner join employee emp
    on sls.employeeId = emp.employeeId
where sls.soldDate >= date('2020', 'start of year')
group by emp.employeeId
order by emp.employeeId
;

-- report whos sold more than 5 cars

select emp.employeeId, emp.firstName, emp.lastName, count(*) as CarsSold
from employee emp inner join sales sls
    on sls.employeeId = emp.employeeId
group by emp.employeeId 
order by emp.employeeId
;
-- where sls.soldDate >= '01/01/2020'
-- having count(*) > 5
-- ???