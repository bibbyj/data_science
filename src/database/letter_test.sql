
drop table if exists letters;
create table letters (
	letter_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
	sndr INTEGER,
	rcvr INTEGER
);
insert into letters(sndr, rcvr)
values
	(2,3),
	(1,2),
	(1,3),
	(1,1),
	(1,2),
	(3,1),
	(2,2);
-----------------------------------------------------------------------------------
drop table if exists users;
create table users (
	user_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
	location TEXT
);
insert into users(location)
values 
	("uk"),
	("france"),
	("ireland");
-----------------------------------------------------------------------------------
drop table if exists results;
create table results (
	user_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
	sent INTEGER,
	rcvd INTEGER
);
-----------------------------------------------------------------------------------
insert into results(sent,rcvd)
values(
	(select count(*) from letters where sndr == 1),
	(select count(*) from letters where rcvr == 1)
);
insert into results(sent,rcvd)
values(
	(select count(*) from letters where sndr == 2),
	(select count(*) from letters where rcvr == 2)
);
insert into results(sent,rcvd)
values(
	(select count(*) from letters where sndr == 3),
	(select count(*) from letters where rcvr == 3)
);

select * from results
