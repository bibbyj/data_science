
import warnings
warnings.filterwarnings('ignore')

import pandas as pd
import spacy
from spacytextblob.spacytextblob import SpacyTextBlob
from random import randint
import matplotlib.pyplot as plt


def scale_model_score(score):
    'Scale sentiment analysis scoring for 0-5 amazon score'
    return round(2.5 + 2.5 * score, 2)


def review_sentiment(sample):
    ''' Analyse the text of a review to determine if the sentiment
    is positive, neutral or negative 
    
    An NLP module (called nlp) must be instantiated before using this 
    function
    
    Function returns a full suite of sentiment analysis and tokens
    '''
    
    # use the NLP instance
    doc = nlp(sample)
    
    # remove unwanted tokens
    filt_token_list = [token for token in doc if not (token.is_stop | token.is_punct | token.is_space)]
    
    # analyse sentiment
    sentiment = doc._.blob.sentiment
    polarity = round(sentiment[0], 2)
    # subjectivity = round(sentiment[1], 2)

    # assign a broad sentiment label based on sentiment value
    if polarity > 0.1:
        review_label = "Positive"
    elif polarity > -0.1:
        review_label = "Neutral"
    else:
        review_label = "Negative"

    # retrieve the tokens that are associated with the predicted sentiment
    positive_words = list()
    positive_tokens = list()
    negative_words = list()
    negative_tokens = list()
    for token_assessment in doc._.blob.sentiment_assessments.assessments:
        word_polarity = token_assessment[1]
        str_token = token_assessment[0][0]
        if word_polarity > 0:
            positive_words.append(str_token)
        elif word_polarity < 0:
            negative_words.append(str_token)
        else:
            pass
    positive_tokens.append(', '.join(set(positive_words)))
    negative_tokens.append(', '.join(set(negative_words)))
    
    # return the full suite of sentiment analysis results
    return filt_token_list, review_label, polarity, positive_tokens, negative_tokens


if __name__ == "__main__":
    
    # read in data from downloaded csv
    df = pd.read_csv("amazon_product_reviews.csv")

    # initialise the spaCy NLP module
    nlp = spacy.load('en_core_web_sm')
    nlp.add_pipe('spacytextblob')

    # slice out fully populated reviews (interesting columns only) 
    reviews_data = df.dropna(subset=['reviews.text'])[["reviews.rating", "reviews.text"]]

    # pick a selection of valid reviews to analyse
    RANDOM_SELECTION_SIZE = 21
    newdf = reviews_data.sample(RANDOM_SELECTION_SIZE)
    
    # overall sentiment label
    newdf["model.label"] = [review_sentiment(review)[1] for review in newdf["reviews.text"]]
    # overall sentiment score
    newdf["model.sentiment"] = [review_sentiment(review)[2] for review in newdf["reviews.text"]]
    # scaled score to work on 0-5 amazon scoring system
    newdf["model.scaled_score"] = [scale_model_score(review_sentiment(review)[2]) for review in newdf["reviews.text"]]
    # the tokens associated with the models predicted sentiment
    newdf["model.mood_tokens"] = [review_sentiment(review)[3] if review_sentiment(review)[2] > 0 
                                else review_sentiment(review)[4] if review_sentiment(review)[2] < 0 
                                else review_sentiment(review)[0]  
                                for review in newdf["reviews.text"]]
    # residual on model score prediction
    newdf["model.residual"] = newdf["model.scaled_score"] - newdf["reviews.rating"]

    review_score = newdf["reviews.rating"]
    model_score = newdf["model.scaled_score"]

    # set up graph axes
    xaxis = [x for x in range(1,RANDOM_SELECTION_SIZE+1)]
    xticks = [x for x in range(0, RANDOM_SELECTION_SIZE+1, 5)]
    yaxis = [y for y in range(0,6,1)]

    # on large datasets, don't display the residual to remove clutter
    if RANDOM_SELECTION_SIZE <= 25:
        plt.plot((xaxis, xaxis), (review_score, model_score), color = "grey", zorder=1)

    # plot the review and model scores
    plt.scatter(xaxis, review_score, label = "Review Rating", zorder=2)
    plt.scatter(xaxis, model_score, color = "orange", label = "Model Predicted Rating", zorder=3)

    # graph customisation
    plt.title("Sentiment Analysis Model Predictions of Amazon Customer Reviews")
    plt.xlabel("Review ID Number")
    plt.xticks(xticks)
    plt.ylabel("Rating")
    plt.yticks(yaxis)
    plt.grid(axis = "y")
    plt.legend()
    plt.show()
    # newdf.head(25)
