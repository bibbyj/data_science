
"""test Flask with this"""
from apikey import API_KEY
import requests

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.sql import text
import os.path

from flask import Flask, render_template
app = Flask(__name__)

db = SQLAlchemy()
db_name = 'sockmarket.db'

# note - path is necessary for a SQLite db!!!
BASE_DIR = os.path.dirname(os.path.abspath(__file__))
db_path = os.path.join(BASE_DIR, db_name)

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + db_path
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
db.init_app(app)

# each table in the database needs a class to be created for it
# this class is named Sock because the database contains info about socks
# and the table in the database is named: socks
# db.Model is required - don't change it
# identify all columns by name and their data type
class Sock(db.Model):
    __tablename__ = 'socks'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String)
    style = db.Column(db.String)
    color = db.Column(db.String)
    quantity = db.Column(db.Integer)
    price = db.Column(db.Float)
    updated = db.Column(db.String)


# get weather by U.S. zip code
API_URL = ('http://api.openweathermap.org/data/2.5/weather?zip={},us&mode=json&units=imperial&appid={}')
ZIP = 86023

def query_weather_api():
    """submit the API query using API_KEY"""
    try:
        data = requests.get(API_URL.format(ZIP, API_KEY)).json()
    except Exception as exc:
        print(exc)
        data = None
    return data


@app.route('/')
def hello():
    return 'Hello World!'

@app.route("/user/<name>")
def user(name):
    T_name = name.title()
    return f"Hello {T_name}"

@app.route("/niceuser/<name>")
def test_template(name):
    return render_template("hello.html", name=name)

@app.route("/weather")
def get_weather():
    resp = query_weather_api()
    # construct a string using the json data items for temp and description
    try:
        text = resp["name"] + " temperature is " + str(resp["main"]["temp"]) + " degrees Fahrenheit with " + resp["weather"][0]["description"] + "."
    except:
        text = "There was an error"
    return text

@app.route('/testcon')
def test_db():
    try:
        print(db.session.query(text('1')).from_statement(text('SELECT 1')).all())
        return '<h1>It works.</h1>'
    except Exception as e:
        # e holds description of the error
        error_text = "<p>The error:<br>" + str(e) + "</p>"
        hed = '<h1>Something is broken.</h1>'
        return hed + error_text
    
@app.route("/sql")
def test_sql():
    try:
        socks = db.session.execute(db.select(Sock)
            # .filter_by(style='knee-high')
            .order_by(Sock.name)).scalars()

        sock_text = '<ul>'
        for sock in socks:
            sock_text += '<li>' + sock.name + ', ' + sock.color + '</li>'
        sock_text += '</ul>'
        return sock_text
    except Exception as e:
        # e holds description of the error
        error_text = "<p>The error:<br>" + str(e) + "</p>"
        hed = '<h1>Something is broken.</h1>'
        return hed + error_text

if __name__== "__main__":
    app.run(debug=True)


