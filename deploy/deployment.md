
Target: Dockerised deployment of a flask application that uses SQL to manage a database

- install Flask
- test Flask deployment
    - Note: add to vscode debug config: "cwd": "${workspaceFolder}/deploy/",
- expand Flask with variable
- use Flask with an API
- use Flask with a database

my-flask-app
   ├── static/
   │   └── css/
   │       └── main.css
   ├── templates/
   │   ├── index.html
   │   └── student.html
   ├── data.py
   └── students.py